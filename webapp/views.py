from django.shortcuts import redirect, render

from webapp.cat import cat


# Create your views here.


def example(request):
    cat['name'] = request.GET.get('name', 'John')
    return redirect(example_2)


def example_2(request):
    return render(request, template_name='index.html', context={'cat': cat})