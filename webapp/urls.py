from django.urls import path

from webapp import views

urlpatterns = [
    path('', views.example, name='index'),
    path('example', views.example_2, name='example'),
]